import React from 'react'

const Child1 = ({ childToParent, onChange }) => {

    const data = "This is data from Child Component to the Parent Component."
    return (
        <div>
            <input type="type" class="form-control" onChange={onChange} />
            <button className="btn btn-success mt-3" type="button" onClick={() => childToParent(data)}>parentToChild</button>
        </div>
    )
}

export default Child1