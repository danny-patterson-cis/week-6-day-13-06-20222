import React from 'react'

const Child = ({ data, count, minusCount, childToParent }) => {
    console.log("Child Render")

    return (
        <div>
            <div>{data}</div>

            <div>{count}</div>
            <div className='mb-3'><button className="btn btn-danger" type="button" onClick={() => minusCount()}>minusCountChild</button></div>

        </div>
    )
}

export default Child